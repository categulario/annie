pub struct Matrix {
    content: Vec<f64>,
    rows: usize,
    cols: usize,
}

impl Matrix {
    fn new(content: Vec<f64>, rows: usize, cols: usize) -> Matrix {
        assert!(content.len() == (rows*cols) as usize);

        Matrix{
            content: content,
            rows: rows,
            cols: cols,
        }
    }

    fn transpose(&mut self) {
        let tmp = self.content.clone();
        let cols = self.cols;
        let rows = self.rows;

        for i in 0..rows {
            for j in 0..cols {
                self.content[j*self.rows+i] = tmp[i*self.cols+j];
            }
        }

        self.cols = rows;
        self.rows = cols;
    }

    fn get_content(&self) -> &[f64] {
        &self.content[..]
    }

    fn get_rows(&self) -> usize {
        self.rows
    }

    fn get_cols(&self) -> usize {
        self.cols
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn transpose_identity() {
        let mut m = Matrix::new(vec![
            1.0, 0.0,
            0.0, 1.0,
        ], 2, 2);

        m.transpose();

        assert_eq!(m.get_content(), [
            1.0, 0.0,
            0.0, 1.0,
        ]);
        assert_eq!(m.get_rows(), 2);
        assert_eq!(m.get_cols(), 2);
    }

    #[test]
    fn transpose_simple() {
        let mut m = Matrix::new(vec![
            1.0, 2.0, 3.0,
            4.0, 5.0, 6.0,
        ], 2, 3);

        m.transpose();

        assert_eq!(m.get_content(), [
            1.0, 4.0,
            2.0, 5.0,
            3.0, 6.0,
        ]);
        assert_eq!(m.get_rows(), 3);
        assert_eq!(m.get_cols(), 2);
    }
}
