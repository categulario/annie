use rand::random;

pub struct NeuralNet {
    pub weights: [f64; 3],
}

impl NeuralNet {
    pub fn new() -> NeuralNet {
        NeuralNet {
            weights: [
                random::<f64>()*2.0-1.0,
                random::<f64>()*2.0-1.0,
                random::<f64>()*2.0-1.0,
            ],
        }
    }

    pub fn train(cycles: u32) {
        // float output[4*1];
        // float error[4*1];
        // float set_input_T[columnas_i*filas_i];
        // float der_sigm[4*1];
        // float error_x_dersigm[4*1];
        // float adjustment[3*1];

        for i in 0..cycles {
            // think(set_input,filas_i,columnas_i,output); // Result: output 4x1
            // resta(set_output_T,filas_oT,columnas_oT,output,error); // Result: error 4x1

            // transponerMatriz(set_input,filas_i,columnas_i,set_input_T); // Result: set_input_T 3x4
            // derivada_sigmoidal(output,4,1,der_sigm); // Result: der_sigm 4x1
            // multiplicar(error,4,1,der_sigm,error_x_dersigm); // Result: error_x_dersigm 4*1

            // dot(set_input_T,columnas_i,filas_i,error_x_dersigm,1,adjustment);

            // suma(adjustment,3,1,self.weights,self.weights);
        }
    }

    // fn think(float set_input[], int filas_i, int columnas_i, float output[]){
        // // Si set_input 4x3 y weights 3x1, entonces: res_mult 4x1
        // float res_mult[filas_i*1];
        // dot(set_input,filas_i,columnas_i,self.weights,1,res_mult);

        // Si res_mult 4x1, entonces: output 4x1
        // sigmoidal(res_mult,filas_i,1,output);
    // }
}

// void dot(float ara[], int filas_a, int columnas_a, float arb[], int columnas_b, float arr[]){
    // for (int i = 0; i < filas_a; i++) {
        // for (int j = 0; j < columnas_b; j++) {
            // float sum = 0;
            // for (int k = 0; k < columnas_a; k++) {
                // sum = (ara[(i*columnas_a)+k] * arb[(k*columnas_b)+j])+sum;
            // }
            // arr[(i*columnas_b)+j] = sum;
        // }
    // }
// }

// void sigmoidal(float ar[], int filas_a, int columnas_a, float arr[]){
    // float exp_value;
    // for(unsigned i=0;i<filas_a;i++){
        // for(unsigned j=0;j<columnas_a;j++){
            // exp_value=exp((double) - ar[(i*columnas_a)+j]);
            // arr[(i*columnas_a)+j]= (1/(1+exp_value));
        // }
    // }
// }

// void derivada_sigmoidal(float ar[], int filas, int columnas, float arr[]){
    // for(unsigned i=0;i<filas;i++){
        // for(unsigned j=0;j<columnas;j++){
            // arr[(i*columnas)+j]= ar[(i*columnas)+j] * (1 - ar[(i*columnas)+j]);
        // }
    // }
// }

// void resta(float aa[], int filas, int columnas, float ab[], float arr[]){
    // for(unsigned i=0;i<filas;i++){
        // for(unsigned j=0;j<columnas;j++){
            // arr[(i*columnas)+j]=aa[(i*columnas)+j]-ab[(i*columnas)+j];
        // }
    // }
// }

// void suma(float aa[], int filas, int columnas, float ab[], float arr[]){
    // for(unsigned i=0;i<filas;i++){
        // for(unsigned j=0;j<columnas;j++){
            // arr[(i*columnas)+j]=aa[(i*columnas)+j]+ab[(i*columnas)+j];
        // }
    // }
// }

// void multiplicar(float aa[], int filas, int columnas, float ab[], float arr[]){
    // for(unsigned i=0;i<filas;i++){
        // for(unsigned j=0;j<columnas;j++){
            // arr[(i*columnas)+j]=aa[(i*columnas)+j]*ab[(i*columnas)+j];
        // }
    // }
// }
