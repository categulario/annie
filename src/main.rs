use annie::nn::NeuralNet;
use annie::matrix::Matrix;

fn main() {
    let nn = NeuralNet::new();

    println!("> Random starting synaptic weights:");
    println!("{:?}", nn.weights);

    let input = [
        0,0,1,
        1,1,1,
        1,0,1,
        0,1,1,
    ];

    // TODO debería ser vector columna
    let output = [0,1,1,0];
    // float set_output_T[4*1];

    // float res[1*1];

    // transponerMatriz(set_output, 1, 4, set_output_T);

    // n.train(input, 4, 3, set_output_T, 4, 1, 20000);

    // cout<<"\n > New synaptic weights after training:\n";
    // n.getWeights();

    // float p[1*3]={1,0,0};
    // n.think(p,1,3,res);
    // cout<<"\n > Considering new situation [1,0,0] -> ? \n";
    // mostrarMatriz(res,1,1);

    // float p2[1*3]={0,0,1};
    // n.think(p2,1,3,res);
    // cout<<"\n > Considering [0,0,1] -> 0 \n";
    // mostrarMatriz(res,1,1);

    // float p3[1*3]={1,1,1};
    // n.think(p3,1,3,res);
    // cout<<"\n > Considering [1,1,1] -> 1 \n";
    // mostrarMatriz(res,1,1);
}
